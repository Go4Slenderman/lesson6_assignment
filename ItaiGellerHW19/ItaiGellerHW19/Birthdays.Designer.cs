﻿namespace ItaiGellerHW19
{
    partial class Birthdays
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Calendar = new System.Windows.Forms.MonthCalendar();
            this.LblNotiflication = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Calendar
            // 
            this.Calendar.CalendarDimensions = new System.Drawing.Size(2, 1);
            this.Calendar.Location = new System.Drawing.Point(0, 0);
            this.Calendar.MaxSelectionCount = 1;
            this.Calendar.Name = "Calendar";
            this.Calendar.TabIndex = 0;
            this.Calendar.DateSelected += new System.Windows.Forms.DateRangeEventHandler(this.Calendar_DateSelected);
            // 
            // LblNotiflication
            // 
            this.LblNotiflication.AutoSize = true;
            this.LblNotiflication.Location = new System.Drawing.Point(12, 171);
            this.LblNotiflication.Name = "LblNotiflication";
            this.LblNotiflication.Size = new System.Drawing.Size(0, 13);
            this.LblNotiflication.TabIndex = 1;
            // 
            // Birthdays
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(452, 261);
            this.Controls.Add(this.LblNotiflication);
            this.Controls.Add(this.Calendar);
            this.Name = "Birthdays";
            this.Text = "Birthdays";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MonthCalendar Calendar;
        private System.Windows.Forms.Label LblNotiflication;
    }
}