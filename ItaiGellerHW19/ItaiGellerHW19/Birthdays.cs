﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ItaiGellerHW19
{
    public partial class Birthdays : Form
    {
        private Dictionary<string, string> dates;
        private string path = "C:/Users/User/Documents/Visual Studio 2013/Projects/ItaiGellerHW19/ItaiGellerHW19";

        public Birthdays(string userName)
        {
            InitializeComponent();
            dates = new Dictionary<string, string>();
            path += "/" + userName + "BD.txt";
            string[] data = null;
            try //Trying the open the file
            {
                data = System.IO.File.ReadAllLines(path);
            }
            catch (IOException ex)
            {
                File.Create(path);
            }
            if(data != null)
            {
                foreach (string line in data) //Parsing each birthday into the dictionary
                {
                    string[] nameDate = line.Split(',');
                    dates.Add(nameDate[1], nameDate[0]);
                }
            }
        }

        private void Calendar_DateSelected(object sender, DateRangeEventArgs e)
        {
            string currDate = Calendar.SelectionRange.Start.ToShortDateString();
            if (dates.ContainsKey(currDate)) //If the date was found in the dictionary
                LblNotiflication.Text = dates[currDate] + " has a birthday at the selected date!";
            else
                LblNotiflication.Text = "Nobody has a birthday at the selected date.";
        }
    }
}
