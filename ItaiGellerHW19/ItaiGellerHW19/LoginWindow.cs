﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ItaiGellerHW19
{
    public partial class Form1 : Form
    {
        private Dictionary<string, string> credentials;
        private const string path = "C:/Users/User/Documents/Visual Studio 2013/Projects/ItaiGellerHW19/ItaiGellerHW19/Users.txt";

        public Form1()
        {
            InitializeComponent();
            MessageBox.Show("Please change the files' paths before continuing");
            credentials = new Dictionary<string, string>();
            string[] data = { "" };
            try //Trying the open the file
            {
                data = System.IO.File.ReadAllLines(path);
            }
            catch (IOException ex)
            {
                MessageBox.Show("The credentials file has failed to open");
                Close();
            }
            foreach (string line in data) //Parsing each user into the dictionary
            {
                string[] tmp = line.Split(',');
                credentials.Add(tmp[0], tmp[1]);
            }
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void BtnLogin_Click(object sender, EventArgs e)
        {
            KeyValuePair<string, string> userLogin = new KeyValuePair<string, string>(TbUserName.Text, TbPassword.Text);
            if(credentials.Contains(userLogin)) //If the user exists in the database
            {
                MessageBox.Show("Successfully logged in!");
                Hide();
                Form birthday = new Birthdays(TbUserName.Text);
                birthday.ShowDialog();
                Close();
            }
            else
                MessageBox.Show("Incorrect user name or password!");
        }
    }
}
